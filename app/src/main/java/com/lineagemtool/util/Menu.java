package com.lineagemtool.util;

public class Menu {
    public static final String OK = "700 600";

    public static final String MENU = "1240 40";
    public static final String CLOSE = MENU;

    public static final String BAG = "1010 40";
    public static final String QUESTS = "1160 40";


    public static final String CHECKIN = "1080 180";

    public static final String LEAGUE = "930 260";

    public static final String CASTLE = "1010 180";
    public static final String FRIENDS = "1010 260";
    public static final String MAIL = "1010 340";

    public static final String ITEM_01 = "550 640";
    public static final String ITEM_02 = "630 640";
    public static final String ITEM_03 = "720 640";
    public static final String ITEM_04 = "800 640";

    public static final String ITEM_045MID = "880 640";

    public static final String ITEM_05 = "960 640";
    public static final String ITEM_06 = "1050 640";
    public static final String ITEM_07 = "1130 640";
    public static final String ITEM_08 = "1200 640";

}
