package com.lineagemtool.util;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.TextView;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Util {
    public static String SNAPSHOT_DELAY = "SNAPSHOT_DELAY";

    public static String AUTO_AWAY = "AUTO_AWAY";
    public static String AUTO_CHECK_OFFLINE = "AUTO_CHECK_OFFLINE";

    public static String AUTO_CHECKIN = "AUTO_CHECKIN";
    public static String AUTO_CHECK_MAIL = "AUTO_CHECK_MAIL";

    public static String AUTO_DONATE = "AUTO_DONATE";


    public static boolean stop = true;

    public static void doAfter(final Runnable task, long after) {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            public void run() {
                if (!stop && task != null)
                    task.run();
            }
        }, after);
    }

    public static void doAfterNoStop(final Runnable task, long after) {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            public void run() {
                if (task != null)
                    task.run();
            }
        }, after);
    }

    public static int currentHour() {
        return  Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    }

    public static int currentMin() {
        return  Calendar.getInstance().get(Calendar.MINUTE);
    }

    @SuppressLint("HandlerLeak")
    public static void updateUI(final Runnable task) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
               if (task != null)
                   task.run();
            }
        });
    }




    public static boolean equals(Bitmap bitmap1, Bitmap bitmap2) {
        ByteBuffer buffer1 = ByteBuffer.allocate(bitmap1.getHeight() * bitmap1.getRowBytes());
        bitmap1.copyPixelsToBuffer(buffer1);

        ByteBuffer buffer2 = ByteBuffer.allocate(bitmap2.getHeight() * bitmap2.getRowBytes());
        bitmap2.copyPixelsToBuffer(buffer2);

        return Arrays.equals(buffer1.array(), buffer2.array());
    }

    public static String toRGB(int color) {
        return Color.red(color) + ", " + Color.green(color) + ", " +Color.blue(color);
    }
}
