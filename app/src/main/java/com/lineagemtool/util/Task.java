package com.lineagemtool.util;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v4.view.InputDeviceCompat;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;
import java.io.OutputStream;

public class Task {


    private static DateFormat sdFormat = new SimpleDateFormat("MM-dd_HH_mm.ss", Locale.US);
    public static void autoAwayWhenKill(final Bitmap bitmap, final boolean takePhoto) {
        Util.doAfter(new Runnable() {
            @Override
            public void run() {
                int color = OCR.getColor(bitmap,1200, 546);
                int r = Color.red(color);
                int g =  Color.green(color);
                int b = Color.blue(color);

                if (r == 255 && b ==148 && (140 < g && g < 180 )) {
                    awayCount = 0;
                    if (takePhoto)
                        OCR.takeScreenshot(" /sdcard/Pictures/"+sdFormat.format(new Date())+".png");
                    goAway();
                }
            }
        }, 0);
    }

    private static int awayCount = 4;
    private static void goAway() {
        if (awayCount < 4) {
            awayCount++;
            tap(Menu.ITEM_07);
            Util.doAfter(new Runnable() {
                @Override
                public void run() {
                    goAway();
                }
            }, 700);
        }
    }

    public static void autoGoHome(final Bitmap bitmap, final boolean takePhoto) {
        if (homeCounttIdle > 0) {
            homeCounttIdle--;
            return;
        }

        Util.doAfter(new Runnable() {
            @Override
            public void run() {

                int color = OCR.getColor(bitmap,120, 25);
                int r = Color.red(color);
                int g =  Color.green(color);
                int b = Color.blue(color);
                boolean red1 = r > 155 && g < 10 && b == 0,
                        red2 = r > 100 && r < 150 && g < 20 && b < 15;

                if (red1 || red2) {
                    return;//save
                }
                if (g == 2 && b == 0) {
                    return;//dialog
                }

                color = OCR.getColor(bitmap,90, 25);
                r = Color.red(color);
                g =  Color.green(color);
                b = Color.blue(color);
                red1 = r > 155 && g < 10 && b == 0;
                red2 = r > 100 && r < 150 && g < 20 && b < 15;
                if (red1 || red2) {
                    homeCount = 0;
                    homeCounttIdle = 50;
                    if (takePhoto)
                        OCR.takeScreenshot(" /sdcard/Pictures/"+sdFormat.format(new Date())+".png");
                    goHome();
                    sendLine("Home");
                }
            }
        }, 0);
    }

    private static int homeCount = 3;
    private static int homeCounttIdle = 0;
    private static void goHome() {
        if (homeCount < 3) {
            homeCount++;
            tap(Menu.ITEM_08);
            Util.doAfter(new Runnable() {
                @Override
                public void run() {
                    goHome();
                }
            }, 700);
        }
    }

    private static void sendLine(final String message) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpsURLConnection conn = null;
                try {
                    URL url = new URL("https://api.line.me/v2/bot/message/push");
                    conn = (HttpsURLConnection) url.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    conn.setRequestProperty("Authorization",
                            "Bearer s9Jrej2uL6i8+y2ZKhycfMpxRnL5UUgH/rHx0yjEfK+pJaUwphhMEUR8gBncT2FAsvOv/IV17UgGhzsp2tXJO17i61QVmThef9E97hfcvjnUJAeIhIUAZIxEnZ0+SJp4D5qQABUnfzPZxcLCP4OfEAdB04t89/1O/w1cDnyilFU=");

                    String input =  "{\"to\":\"Ue6c33b06d33262d8f202b000162c72cf\",\"messages\": [{\"type\": \"text\", \"text\": \"" +message+ "\"}]}";

                    OutputStream os = conn.getOutputStream();
                    os.write(input.getBytes());
                    os.flush();
                    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                    String output;
                    while ((output = br.readLine()) != null) {
                        System.out.println(output);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
            }
        }).start();
    }

    public static void autoClick() {
        inAutoClick = !inAutoClick;
        if (inAutoClick)
            autoClick0();
    }

    private static boolean inAutoClick = false;
    private static void autoClick0() {
        if (inAutoClick) {
            tap(Menu.ITEM_045MID);
            Util.doAfterNoStop(new Runnable() {
                @Override
                public void run() {
                    autoClick0();
                }
            }, 700);
        }
    }

    public static void checkin() {
        tap(new String[]{
            Menu.MENU, Menu.CHECKIN,
            Menu.CLOSE,Menu.CLOSE});
    }

    public static void checkMail() {
        tap(new String[]{
            Menu.MENU, Menu.MAIL, Checkin.TAKE_ALL,
            Menu.CLOSE, Menu.CLOSE});
    }

    public static void checkQuest() {
        tap(new String[]{
                Menu.QUESTS, Quest.DAILY, Quest.DAILY_1ST,
                Menu.CLOSE, Menu.CLOSE});
    }

    public static void donate10() {
        tap(new String[]{
                Menu.MENU, Menu.LEAGUE,
                League.DONATE,  League.DONATE_10, League.DONATE_CONFIM,
                League.DONATE_CLOSE, Menu.CLOSE, Menu.CLOSE});
    }

    public static void checkCoin() {
        tap(new String[]{
                Menu.MENU, Menu.LEAGUE,
                League.DONATE_GIFT, Menu.CLOSE, Menu.CLOSE, Menu.CLOSE});
    }

    public static void tap(int x, int y) {
        _triggerTouch(x + " " + y);
    }

    public static void tap(Point p) {
        _triggerTouch(p.x + " " +p. y);
    }

    public static void tap(String pos) {
        _triggerTouch(pos);
    }

    private static void tap(String[] list) {
        try {
            for (String pos : list) {
                _triggerTouch(pos);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private static void _triggerTouch(String pos) {
        try {
            Process process = Runtime.getRuntime().exec("su");
            DataOutputStream os = new DataOutputStream(process.getOutputStream());
            String cmd = "/system/bin/input tap " + pos + "\n";
            os.writeBytes(cmd);
            os.writeBytes("exit\n");
            os.flush();
            os.close();
            process.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SecurityException se) {
            se.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private static void _triggerTouch2(String pos) {
        String[] point = pos.split(" ");
        int x = Integer.parseInt(point[0]);
        int y = Integer.parseInt(point[1]);

//        long downTime = SystemClock.uptimeMillis();
//        Instrumentation mInst = new Instrumentation();
//        mInst.sendPointerSync(MotionEvent.obtain(downTime, downTime, MotionEvent.ACTION_DOWN, x, y, 0));
//        mInst.sendPointerSync(MotionEvent.obtain(downTime, downTime, MotionEvent.ACTION_UP, x, y, 0));

        tap(x, y);

    }

    private static EventInput input;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private static void tap2(int x, int y) {
        if (input == null) {
            try {
                input = new EventInput();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            input.injectMotionEvent(InputDeviceCompat.SOURCE_TOUCHSCREEN, 0,
                    SystemClock.uptimeMillis(), x, y, 1.0f);
            input.injectMotionEvent(InputDeviceCompat.SOURCE_TOUCHSCREEN, 1,
                    SystemClock.uptimeMillis(), x, y, 1.0f);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
