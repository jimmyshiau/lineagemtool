package com.lineagemtool.util;

import android.graphics.Bitmap;
import android.graphics.Point;

import java.util.TimerTask;

public class Checkin {

    public static final String TAKE_ALL = "1070 660";

    private static Integer[] TIMES = new Integer[]{9, 12, 18, 21};

    private static Point MENU_RED_DOT = new Point(1253, 28);
    private static boolean firstStart = true;
    public static void firstCheckMail(final Bitmap bitmap) {
        if (Util.stop)
            return;

        if (firstStart) {
            firstStart = false;
            int menuRedDot = OCR.getColor(bitmap, MENU_RED_DOT);
            if (menuRedDot == -54742) {
                Task.checkMail();
            }
        }
    }


    public static void autoCheckMail() {
        Integer curH = Util.currentHour();
        Integer curM = Util.currentMin();
        Integer nextH = TIMES[0];
        for (Integer h : TIMES) {
            if (curH < h) {
                nextH = h;
                break;
            }
        }

        int doAfter = nextH - curH;
        if (doAfter <= 0) {
            doAfter += 24;
        }

        Util.doAfterNoStop(new TimerTask() {
            @Override
            public void run() {
                Task.checkMail();
                autoCheckMail();
            }
        },  (doAfter * 60 - curM + 5) * 60 * 1000);


    }

    public static void autoCheckQuest() {
        doWhen(8, new TimerTask() {
            @Override
            public void run() {
                Task.checkQuest();
                autoCheckQuest();
            }
        });
    }

    public static void autoCheckin() {
        doWhen(5, new TimerTask() {
            @Override
            public void run() {
                Task.checkin();
                Task.donate10();
                autoCheckin();
            }
        });
    }

    public static void autoCheckCoin() {
        doWhen(6, new TimerTask() {
            @Override
            public void run() {
                Task.checkCoin();
                autoCheckCoin();
            }
        });
    }

    private static void doWhen(int h, Runnable task) {
        Integer curH = Util.currentHour();
        Integer curM = Util.currentMin();

        int doAfter = h - curH;
        if (doAfter <= 0) {
            doAfter += 24;
        }

        Util.doAfterNoStop(task, (doAfter * 60 - curM + 10) * 60 * 1000);
    }
}
