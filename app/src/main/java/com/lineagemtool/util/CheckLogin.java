package com.lineagemtool.util;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;


public class CheckLogin {

    private static int BLUE_BUTTON_COLOR = -14798515;
    private static int LOGIN_SCREEN_COLOR = -6367511;



    private static Point LOGIN_SCREEN = new Point(188, 656);

    private static Point CENTER_BUTTON = new Point(555, 590);
    private static Point LOGIN_BUTTON = new Point(1002, 660);
    private static Point AUTO_BUTTON = new Point(974, 510);

    private static Long last;
    private static boolean newLogin = false;
    public static void checkOffline(final Bitmap bitmap) {
        Util.doAfter(new Runnable() {
            @Override
            public void run() {
                int centerBtn = OCR.getColor(bitmap, CENTER_BUTTON);
                if (centerBtn == BLUE_BUTTON_COLOR) {
                    Task.tap(CENTER_BUTTON);
                }

                int loginScreen = OCR.getColor(bitmap, LOGIN_SCREEN);
                int loginBtn = OCR.getColor(bitmap,LOGIN_BUTTON);
                if (loginScreen == LOGIN_SCREEN_COLOR && loginBtn == BLUE_BUTTON_COLOR) {
                    Task.tap(LOGIN_BUTTON);
                    newLogin = true;
                    last =System.currentTimeMillis();
                }

                //&& (System.currentTimeMillis() - last) > 10000) {//try after 10 sec
                if (newLogin) {//try after 10 sec
                    if ((System.currentTimeMillis() - last) < 600000) {//do in 10 min
                        if (!isAuto(bitmap))
                            Task.tap(AUTO_BUTTON);
                    } else
                        newLogin = false;
                }
            }
        }, 0);
    }

    private static boolean isAuto(Bitmap bitmap) {
        return Color.red(OCR.getColor(bitmap, AUTO_BUTTON)) > 200
                || Color.red(OCR.getColor(bitmap, new Point(979,496))) > 200
                || Color.red(OCR.getColor(bitmap, new Point(946,492))) > 200;
    }
}
