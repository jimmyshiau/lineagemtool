package com.lineagemtool;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Build;

public class ShotApplication extends Application {

    public SharedPreferences preferences;
    @Override
    public void onCreate() {
        super.onCreate();
        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);
    }

    Long _prev;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setResult(Image image) {
        Long now = System.currentTimeMillis();
        if (_prev == null || (now - _prev) > 1500) {
            _prev = now;
        } else return;

        int width = image.getWidth();
        int height = image.getHeight();
        final android.media.Image.Plane[] planes = image.getPlanes();
        final java.nio.ByteBuffer buffer = planes[0].getBuffer();
        int pixelStride = planes[0].getPixelStride();
        int rowStride = planes[0].getRowStride();
        int rowPadding = rowStride - pixelStride * width;
        bitmap = Bitmap.createBitmap(width+rowPadding/pixelStride, height, Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(buffer);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0,width, height);
    }

    Bitmap bitmap;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Bitmap getResult(){
        return bitmap;
    }
}
