package com.lineagemtool;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import com.lineagemtool.util.CheckLogin;
import com.lineagemtool.util.Checkin;
import com.lineagemtool.util.OCR;
import com.lineagemtool.util.Task;
import com.lineagemtool.util.Util;


public class FloatWidgetService extends Service {
    private WindowManager mWindowManager;
    private View mFloatingWidget;

    public Long initTime;

    public FloatWidgetService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private boolean _autoClick = false;
    @Override
    public void onCreate() {
        super.onCreate();

        initTime = System.currentTimeMillis();

        mFloatingWidget = LayoutInflater.from(this).inflate(R.layout.layout_floating_widget, null);
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.TOP | Gravity.LEFT;

        params.x = (int) getPref().getFloat("x", 1230);
        params.y = (int) getPref().getFloat("y", 200);
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(mFloatingWidget, params);

        final View collapsedView = mFloatingWidget.findViewById(R.id.collapse_view);
        final View expandedView = mFloatingWidget.findViewById(R.id.expanded_container);


        ImageView closeButtonCollapsed = (ImageView) mFloatingWidget.findViewById(R.id.close_btn);
        closeButtonCollapsed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopSelf();
            }
        });
        ImageView closeButton = (ImageView) mFloatingWidget.findViewById(R.id.close_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collapsedView.setVisibility(View.VISIBLE);
                expandedView.setVisibility(View.GONE);
            }
        });

        final ImageView playBtn = (ImageView) mFloatingWidget.findViewById(R.id.play_btn);
        final ImageView stopBtn = (ImageView) mFloatingWidget.findViewById(R.id.stop_btn);
        final Button autoBtn = (Button) mFloatingWidget.findViewById(R.id.auto_click);

        playBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                stopBtn.setVisibility(View.VISIBLE);
                playBtn.setVisibility(View.GONE);

//                collapsedView.setVisibility(View.VISIBLE);
//                expandedView.setVisibility(View.GONE);

                _start();
            }
        });


        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playBtn.setVisibility(View.VISIBLE);
                stopBtn.setVisibility(View.GONE);
                _stop();
            }
        });

        autoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autoBtn.setText(_autoClick ? "A": "X" );
                Task.autoClick();
                _autoClick = !_autoClick;
            }
        });

        mFloatingWidget.findViewById(R.id.root_container).setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        float x = event.getRawX();
                        float y = event.getRawY();
                        int Xdiff = (int) (x - initialTouchX);
                        int Ydiff = (int) (y - initialTouchY);
                        //logPos((int)x, (int)y);

                        getPref().edit()
                            .putFloat("x", x).putFloat("y", y).apply();

                        if (Xdiff < 3 && Ydiff < 3) {
                            if (isViewCollapsed()) {
                                collapsedView.setVisibility(View.GONE);
                                expandedView.setVisibility(View.VISIBLE);
                            }
                        }
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);
                        mWindowManager.updateViewLayout(mFloatingWidget, params);
                        return true;
                }
                return false;
            }
        });

        Util.stop = !isStart();
        if (!Util.stop) {
            stopBtn.setVisibility(View.VISIBLE);
            playBtn.setVisibility(View.GONE);

            collapsedView.setVisibility(View.GONE);
            expandedView.setVisibility(View.VISIBLE);

            _start();
        }
    }



    private ShotApplication getShotApplication() {
        return (ShotApplication)getApplication();
    }

    private void log(String text) {
        log(text, Color.BLACK);
    }

    private void logColor(int color) {
        ImageView closeButtonCollapsed = (ImageView) mFloatingWidget.findViewById(R.id.close_btn);
        closeButtonCollapsed.setBackgroundColor(color);
    }

     private void logImage(final Bitmap bitmap) {
        Util.updateUI(new TimerTask() {
            @Override
            public void run() {
                final ImageView img = mFloatingWidget.findViewById(R.id.img);
                img.setVisibility(bitmap != null ? View.VISIBLE: View.GONE);
                if (bitmap != null)
                     img.setImageBitmap(bitmap);

            }
        });
    }

    private void logScrean(Bitmap bitmap) {
        final ImageView img = mFloatingWidget.findViewById(R.id.img);
        //Bitmap bitmap = getShotApplication().getResult();
        img.setVisibility(bitmap != null ? View.VISIBLE: View.GONE);
        if (bitmap != null)
            img.setImageBitmap(Bitmap.createBitmap(bitmap,120,  15, 50, 50));
    }

    private void log(final String text, final int color) {
       Util.updateUI(new TimerTask() {
           @Override
           public void run() {
           final TextView log = (TextView) mFloatingWidget.findViewById(R.id.log);
           log.setTextColor(color);
           log.setText(text);
           }
       });
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();
        Checkin.autoCheckMail();
        Checkin.autoCheckin();
        Checkin.autoCheckCoin();
        Checkin.autoCheckQuest();
        return START_STICKY;
    }

    Bitmap bitmap;
    private void startTimer() {

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (Util.stop)
                    return;

                bitmap = OCR.takeScreenshot();
//                bitmap = getShotApplication().getResult();

                Task.autoAwayWhenKill(bitmap, true);
                Task.autoGoHome(bitmap, true);
                CheckLogin.checkOffline(bitmap);
                Checkin.firstCheckMail(bitmap);

//                Util.updateUI(new Runnable() {
//                    public void run() {
//                        logScrean(bitmap);
//                    }
//                });

                log(_toTime(System.currentTimeMillis() - initTime));

            }
        }, 1000, 3500);
    }

    double min = 60, hour = 3600, day = 86400;

    private String _toTime(long time) {
        long totalSec = time/1000;
        int d=(int)(totalSec/day);
        int h =(int)((totalSec/hour)%24);
        int m =(int)((totalSec/min)%60);
        return d+"d"+h+":"+ m+":"+ (totalSec%60);
    }

    public SharedPreferences getPref() {
        return getShotApplication().preferences;
    }


    private boolean isStart() {
        return getPref().getBoolean("start", false);
    }
    private void _stop() {
        log("");
        Util.stop = true;
        getPref().edit().putBoolean("start", false).apply();

//        Intent intent = new Intent("com.cap");
//        sendBroadcast(intent);

//        Intent i = new Intent("auto");
//        i.setClass(this, MainActivity.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(i);
    }

    Timer timer;


    private void _start() {
        getPref().edit().putBoolean("start", true).apply();

        Util.stop = false;

    }

    private void logPos(final int x, final int y) {
        log(x + "\n" + y);
    }

    private boolean isViewCollapsed() {
        return mFloatingWidget == null || mFloatingWidget.findViewById(R.id.collapse_view).getVisibility() == View.VISIBLE;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFloatingWidget != null)
            mWindowManager.removeView(mFloatingWidget);
    }
}