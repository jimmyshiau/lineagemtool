package com.lineagemtool;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.graphics.Point;
import android.hardware.display.DisplayManager;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static android.media.ImageReader.newInstance;

public class MainActivity extends AppCompatActivity {
    private static final int APP_PERMISSION_REQUEST = 102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, APP_PERMISSION_REQUEST);
        } else {
            initializeView();
            initOCR();
            initCapture();

//            if (getIntent().getAction().equals("auto"))
//                moveTaskToBack (true);
        }
    }

    @Override
    protected void onDestroy() {
//        unregisterReceiver(receiver);
        super.onDestroy();
    }

    private void initCapture() {
        mMediaProjectionManager = (MediaProjectionManager)getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        doCapture();
//        MyReceiver.act = this;
//        registerReceiver(receiver, new IntentFilter("com.cap"));

        getShotApplication().preferences.edit().putBoolean("start", false).apply();
    }


    private void doCapture() {
//        startActivityForResult(mMediaProjectionManager.createScreenCaptureIntent(), REQUEST_MEDIA_PROJECTION);
    }


    private ShotApplication getShotApplication() {
        return (ShotApplication)getApplication();
    }

    private void initOCR() {
        String toPath = "/storage/emulated/0/download/tesseract/tessdata/";
        if (!new File(toPath).exists())
            copyAsset(getAssets(), "chi_tra.traineddata", toPath);
    }

    private static boolean copyAsset(AssetManager assetManager, String fromAssetPath, String toPath) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(fromAssetPath);
            new File(toPath).mkdirs();
            toPath += fromAssetPath;
            new File(toPath).createNewFile();
            out = new FileOutputStream(toPath);
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    private void initializeView() {
        Button mButton = (Button) findViewById(R.id.createBtn);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(new Intent(MainActivity.this, FloatWidgetService.class));
//                moveTaskToBack (true);
                finish();
            }
        });
    }


    Long _lastTime;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == APP_PERMISSION_REQUEST && resultCode == RESULT_OK) {
            initializeView();
        } else if (requestCode == REQUEST_MEDIA_PROJECTION && resultCode == RESULT_OK) {
            long now = System.currentTimeMillis();
            if (data != null && (_lastTime == null || (now - _lastTime) > 2000)){
                _lastTime = now;
                storeDate(resultCode, data);
            }
        } else {
            Toast.makeText(this, "Draw over other app permission not enable.", Toast.LENGTH_SHORT).show();
        }
    }


    void storeDate(int resultCode, Intent data) {
        WindowManager mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Point point = new Point();
        mWindowManager.getDefaultDisplay().getSize(point);

        DisplayMetrics metrics = new DisplayMetrics();
        mWindowManager.getDefaultDisplay().getMetrics(metrics);

        MediaProjection mMediaProjection = mMediaProjectionManager.getMediaProjection(resultCode, data);

        final ImageReader mImageReader = newInstance(point.x, point.y, 0x1, 2); //ImageFormat.RGB_565
        mMediaProjection.createVirtualDisplay("screen-mirror",
            point.x, point.y, metrics.densityDpi, DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                mImageReader.getSurface(), null, null);

        android.os.HandlerThread mCheckThread = new android.os.HandlerThread("CheckHandler");
        mCheckThread.start();
        android.os.Handler mCheckHandler = new android.os.Handler(mCheckThread.getLooper());

        mImageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {
                image = reader.acquireLatestImage();
                if (image != null) {
                    getShotApplication().setResult(image);
                    image.close();
                }
            }
        }, mCheckHandler);

    }
    Image image;

    private int REQUEST_MEDIA_PROJECTION = 1;
    private MediaProjectionManager mMediaProjectionManager;

    MyReceiver receiver;
    public static class MyReceiver extends BroadcastReceiver {
        MyReceiver() {
            super();
        }
        static MainActivity act;
        @Override
        public void onReceive(Context context, Intent intent) {
           if (act != null)
               act.doCapture();
        }
    }
}